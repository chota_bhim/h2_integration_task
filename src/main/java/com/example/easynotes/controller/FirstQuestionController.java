package com.example.easynotes.controller;

import com.example.easynotes.exception.BadRequest;
import com.example.easynotes.exception.ErrorHandler;
import com.example.easynotes.model.Vehicle;
import com.example.easynotes.service.FirstQuestionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
public class FirstQuestionController {

    @Autowired
    FirstQuestionService firstQuestionService;

    @GetMapping
    public void div()
    {
        System.out.println("div");
    }


    @PostMapping("/vehicle-api/v1/vehicles/vehicle")
    public ResponseEntity<?> getId( @RequestBody Vehicle vehicle) {
        System.out.println("Data" + vehicle.toString());
        Logger log = LoggerFactory.getLogger(FirstQuestionController.class);
        log.info("Request Payload {}", vehicle.toString());
        if(vehicle.getTransmissionType() == null || vehicle.getModel() == null || vehicle.getMake() == null || vehicle.getYear() == 0) {
            throw  new ErrorHandler(HttpStatus.INTERNAL_SERVER_ERROR, "Payload canot be null");
        }
        else if (!(vehicle.getTransmissionType().equals("MANUAL")) && !(vehicle.getTransmissionType().equals("AUTO"))) {
            throw new BadRequest(HttpStatus.BAD_REQUEST, "Transmission type should be MANUAL or AUTO");
        }else{
            return new ResponseEntity<>(firstQuestionService.getId(vehicle), HttpStatus.OK);
        }
    }

    }

