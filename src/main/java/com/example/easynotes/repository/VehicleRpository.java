package com.example.easynotes.repository;

import com.example.easynotes.model.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VehicleRpository extends JpaRepository<Vehicle, Long> {

}