package com.example.easynotes.service;

import com.example.easynotes.exception.ErrorHandler;
import com.example.easynotes.model.Vehicle;
import com.example.easynotes.repository.VehicleRpository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
public class FirstQuestionService {

@Autowired
    VehicleRpository vehicleRpository;
    @Async
    public Object getId(Vehicle vehicle) {
        Logger log = LoggerFactory.getLogger(FirstQuestionService.class);
        log.info("Request Payload {}", vehicle.toString());
        log.info("Vehicle ID: {}", vehicle.getVin());
        vehicleRpository.save(vehicle);
        log.info("Vehicle Post Data: {}", vehicle.toString());
        System.out.println("{\n  \"vin\": \""+vehicle.getVin()+"\",\n  \"year\": "+vehicle.getYear()+",\n  \"make\": "+vehicle.getMake()
                +",\n  \"model\": "+vehicle.getModel()+",\n  \"trasmissionType\": "+vehicle.getTransmissionType()+"\"\n}");

        return CompletableFuture.completedFuture(vehicle.getVin());



    }
}

